import React from 'react';


// Desconstructuring
const Todos = ({ todos, deleteTodo }) => {

  const todoList = todos.length ? (
    todos.map(todo => {
      return (
        <div key={todo.id}>
          <span>{todo.content}</span>
          <button onClick={() => {deleteTodo(todo.id)}}>Delete</button>
        </div>
      )
    })
  ) : (
    <p>You not todo, very nice ;-p</p>
  )

  return (
    <div>
      {todoList}
    </div>
  )
}

export default Todos;
