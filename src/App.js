import React, { Component } from 'react';

import Todos from './components/Todos'
import AddTodo from './components/AddTodo'

class App extends Component {
  state = {
    todos: [
      {id: 1, content: 'Ir na padaria'},
      {id: 2, content: 'Comprar pão e Donuts'},
      {id: 3, content: 'Fazer café'},
      {id: 4, content: 'E começar a Codar'},
    ]
  }

  deleteTodo = (id) => {
    const todos = this.state.todos.filter(todo => {
      return todo.id !== id
    })

    // if todo.id iqual id
    this.setState({
      todos: todos
    })
  }

  addTodo = (todo) => {
    // no database, generate id automatic
    todo.id = Math.random() * 10;
    // [...] clone last state
    let todos = [...this.state.todos, todo]

    // now set new todo
    this.setState({
      todos: todos // or todos
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          TODO - REACT
        </header>
        {/* passing the state via props */}
        <AddTodo addTodo={this.addTodo} />
        <hr />
        {/* passing the state via props */}
        <Todos todos={this.state.todos} deleteTodo={this.deleteTodo} />
      </div>
    );
  }
}

export default App;
